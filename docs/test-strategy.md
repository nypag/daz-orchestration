# DA Z - Testing Strategy

This is the testing strategy file of the of the DA Z - Eternity project, which shows which approaches  
and test methods are choosen to test the application.

In general the DA Z – Eternity project is departed in two projects. The first one would be the frontend  
which is written in ReactJs and the second would be the backend, which is written in PHP Laravel.  
The tests should be made in the frontend and backend.

## Introduction in the tests definition

The following test methods will be used during the project:

1. Unittests
2. End-to-End

We will need unit tests, because with this each methods could be tested individually.  
Unit tests are essential and therefore cannot be left out of the testing strategy.  
Unit tests will be a important testing component in our test strategy.

System testing is done as end-to-end testing is proposed to us.

## Tools and utilities

| Tools                   | Description        | Usage    |
| ----------------------- | ------------------ | -------- |
| Cypress                 | End-to-End testing | Frontend |
| Jest                    | Unittest           | Frontend |
| Testing Library React   | Unittest           | Frontend |
| Laravel Testing Library | Unittest           | Backend  |

The following tools are required to test the frontend. Jest and Testing Library Reactare recommended by the official React site for testing.  
 Cypress is used for end-to-end testing and is used
by default in Noser Young for end-to-end testing.  
 The laravel testing library will be used to test the
backend.  
The testing of sending emails will be also done through the laravel testing library and won’t use any external library.

## Errorhandling

Errorhandling should be built into every function/api call that could possibly crash the system.  
 This rule applies to both the code in React and in Laravel.

## Sonarqube

1. Security Hotspots
   1. medium and critical => review
2. No Vulnerabilities
3. Code Smells
   1. No blocker code smells
   2. No criticals code smells
   3. No major code smells
4. No Bugs

## Mobile device testing

Every feature that is built in is also checked for bugs for the mobile devices.  
This is done through the Device Tools feature that is provided to us by our browsers.  
An example would be the Device Toolbar from Google Chrome.
