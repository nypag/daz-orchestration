# Definition of Done
This document describes the so-called definition of done, which the team has put together to have a shared understanding of when a story(Srum) is done.

* Stories have to be assigned before they're started
*  For each story a new branch is created with a fitting name(Number, titel, etc.)
    - feature/WB-42-the-feature-todo
    - improvement/WB-42-improve-everything
    - bugfix/WB-42-fix-some-thing
* Acceptance criteria of the story are fulfilled
* Code is executable and tested
* Tests are automatically executed and successful
* Fitting comments are added where useful
    - In case more Information is needed to understand parts of code
        e.g `/*textGeometry font only supports the latin alphabet. Examples of not working text: Emojis, Japanese, Hindi*/` 
    - Comments are added in JavaDoc format according to Coding Conventions
* Coding Conventions according to the [Coding Tools][1] are followed
* Unused parts (e.g. imports) and debugging tools (e.g. logs, console outputs, etc.) are removed
* Code-Review is done before moving story to "Done"
* Naming Conventions according to [Coding Conventions][2] are followed
* Code is merged into main branch
[1]:../docs/tooling.md
[2]:../docs/images/prettierrc.PNG