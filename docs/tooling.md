# Tooling

This is the instructions file of the DA Z-Eterity Project.  
This file is made for the developers of the DA Z-Eterity Project Team to help them install and download the choosen coding tools for this project.  
This file is written in the Markdown language and contains multiple images and code snippets.

In general the project uses Visual Studio Code.

## How it works and what it does

Prettier enforces a consistent code style (i.e. code formatting that won’t affect the AST) across your entire codebase because it disregards the original styling  
by parsing it away and re-printing the parsed AST with its own rules that take the maximum line length into account, wrapping code when necessary.

The reason why we use Prettier in our project is the flexibility of the tool. It can format mulitple programming languages and it also integrates with the most editors.  
This save us time and energy both when we write code, because it auto saves the changes and when we sit in a code review meeting because all the styling questions are dropped.

We can make the formatting of the code a bit more team or company specific by changing the formatting rules of Prettier. We can define the rules in the .prettierrc file.  
However, this work has been taken from you by the dev team and you don't have to worry about anything except for the introduction.

As explained above, first prettier tries to parse the code and afterwards it writes it again, but this time it is nicer and formatted according to the rules in .prettierrc.  
For PHP you had to install an additional plug-in to make the formatting work.

## Frontend

---

1.  Please enter in VS Code Terminal the following code "npm install --save-dev --save-exact prettier"

2.  Go in File>Preferences>Settings and please choose "Prettier" as the default formatter. Please check the box "formatOnSave" also.

    ![](./images/vsCodeFormatter.png)

3.  Restart visual studio code to see the changes

## Backend

---

1. Please enter in VS Code Terminal the following code "npm install --save-dev prettier @prettier/plugin-php"

   ![](./images/prettier-php.png)
