# DA Z: Eternity - Getting started

This documents describes how to get the project `Eternity` up and running.

## What is this all about?

TODO

## Setup

### Prerequisites

Make sure that you have the following installed:

* Docker (including compose)
* Unzip
* PHP 7.4
* PHP-CLI
* PHP-XML
* PHP-MBSTRING
* PHP-MYSQL
* Composer


#### Linux

When using Linux, e.g. WSL2, use the following commands to install the prerequisites:

* `sudo apt update`
* `sudo apt install php7.4 php-cli php-xml php-mbstring php-mysql unzip`
* `cd ~`
* `curl -sS https://getcomposer.org/installer -o composer-setup.php`
* `sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer`

### Database

Simply run docker compose to get the database as well phpMyAdmin running: `docker-compose up -d`.

You may then access the database directly using `localhost:3306` or administer it using phpMyAdmin under `http://localhost:8443`.

### Backend

First, clone the backend, e.g. `git clone git@bitbucket.org:nypag/daz-backend.git`.
Then change to the backend directory, `cd daz-backend`.

Install the necessary dependencies using composer: `composer install`.

Copy (and eventually) adapt the `.env` file: `cp .env.example .env`.

Migrate the database: `php artisan migrate`.

Optionally seed the database with test data:  `php artisan db:seed`.

Run the backend using `php artisan serve`.

You can then access the backend under `http://localhost:8000`.

## Frontend

Make sure you are in the folder where you cloned the `daz-orchestration` repository.

Then, clone the frontend, e.g. `git clone git@bitbucket.org:nypag/daz-frontend.git`.

Install the necessary dependencies using npm: `npm install`.

Start the frontend using `npm start`.

You can then access the frontend under `http://localhost:3000`.

## Credentials

See `.env` file in this repository for credentials.

## Entrypoints

### Frontend

See `daz-frontend/src/routing/RouteProvider.tsx` for details. Access the routes
using `http://localhost:3000`.

### Backend

See `daz-backend/routes/api.php` for details. Access the routes using
`http://localhost:8000`.